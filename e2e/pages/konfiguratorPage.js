let KonfiguratorPage = function () {
  let EC = protractor.ExpectedConditions,
    width = element(by.xpath("//span[@data-submit-key='150']")),
    height = element(by.xpath("//span[@data-submit-key='216']")),
    color = element(by.xpath("//span[@data-submit-key='sonoma']")),
    nextButton = element(by.id("submitStepButtonId")),
    turenart = element(by.xpath("//span[@data-submit-key='Ohne']"));

  this.getConfiguratorPage = async function () {
    await browser.get("/konfigurator/dreamer");
  };
  this.clickWidth = async function () {
    browser.wait(EC.elementToBeClickable((width)), 15000);
    await width.click();
  };
  this.clickHeight = async function () {
    browser.wait(EC.elementToBeClickable((height)), 15000);
    await height.click();
  };
  this.clickColor = async function () {
    browser.wait(EC.elementToBeClickable((color)), 15000);
    await color.click();
  };
  this.clickNextStep = async function () {
    browser.wait(EC.elementToBeClickable((nextButton)), 15000);
    await nextButton.click();
  };
  this.clickTurenart = async function () {
    browser.wait(EC.elementToBeClickable((turenart)), 15000);
    await turenart.click();
  };
}
module.exports = new KonfiguratorPage();