Feature: The user is able to set the feature of the product
  As a web user,
  The user wants to set the features of the product

  Scenario: The user set the features of the product successfully
    Given The user goes to Konfigurator Page
    When The user set all features of the product
    And The user goes to the next step
    Then The user should see all features information of the product