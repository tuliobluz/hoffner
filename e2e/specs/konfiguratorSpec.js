let konfiguratorPage = require('../pages/konfiguratorPage.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1200);

Given('The user goes to Konfigurator Page', async function () {
    await browser.waitForAngularEnabled(false);
    await konfiguratorPage.getConfiguratorPage();
});

When('The user set all features of the product', async function () {
    await konfiguratorPage.clickWidth();
    await konfiguratorPage.clickHeight();
    await konfiguratorPage.clickColor();
    await konfiguratorPage.clickNextStep();

});

When('The user goes to the next step', async function () {
});

Then('The user should see all features information of the product', async function () {

});

